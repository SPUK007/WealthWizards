
SP Comment: Part1 Completed using VisualStudio tool in WebDriver IO

Part 1: Create a JS npm project with cypress.io

Create a JS npm project where you can use Cypress.io to automate the below scenario:

1.	Go to https://www.wealthwizards.com/  
2.	Check that About Us dropdown has 3 values: “Who we are”, “Careers” and “Contact”
3.	Click on “Careers” - you should get redirected to https://www.wealthwizards.com/careers 
4.	Check the 5  WW values
5.	Check that the page contains “View jobs” button
6.	Click “View jobs” button   - you should get redirected to https://www.wealthwizards.com/job-vacancies  
7.	Find “Lead Quality Engineer” role  and click “Find out more” button
8.	Validate the page 

-----------------------------------------------------------------------------

SP Comment: Part2 Completed using Timeline Reporter which generates test result in HTML Format

Part 2: Test Reporting
Add functionality to create a html report from your cypress test run.

--------------------------------------------------------------------------------

SP Comment: Not Started

Part 3: Visual test
Integrate with a Visual testing tool (Percy/Applitools) to validate the Lead Quality Engineer role page 
•	You can register for a free account for Percy (https://percy.io/ ) or Applitools (https://applitools.com/ ). 
•	Run the test against 2 browsers (Chrome/Edge) and 3 resolutions (320px, 768px, 1366px).
•	Take full page screenshot from Percy/Applitools and attach it into the repo/PR/Readme when you submit the exercise.

-----------------------------------------------------------------------------------

SP Comment: Made an attempted to perform the CI in direct GitLab but managed to add just skeleton structe to the "*.Yaml" file - Not Completed

Part 4: CI Integration
How would be integrate this test to run into CI - could you draft a Jenkinsfile? Or another CI pipeline that can be integrated with your Github project? 
•	Note: We use Jenkins as our CI tool and that’s the reason we started with Jenkinsfile, but feel free to use any other available option to integrate your project into a CI tool. 
•	We understand you might not be able to validate the CI pipeline, but even a draft would give us an idea over your CI understanding. 
•	Where you don't know the Jenkins/Groovy technical format, it’s sufficient to add a comment and explain your intention for that step

-------------------------------------------------------------------------------------------------

SP Comment: Not Started

Part 5: Cucumber 
Create a Cucumber Feature file to test 2021 income tax rules for UK (https://www.gov.uk/income-tax-rates ). 
•	Gherkin Reference - Cucumber Documentation: https://cucumber.io/docs/gherkin/reference/ 
•	Note! We understand the project won’t have the step definitions for the feature file to work, we are only interested into how the scenarios are created at this point.
o	Tests to be created for a single individual eligible for income tax and Personal Allowance 
o	Savings / Dividends to be ignored. Only taxable income to be considered.