class ElementUtil{

    doClick(element)
    {
        element.waitForDisplayed();
        element.click();
    }

    doIsDisplayed(element)
    {
        element.waitForDisplayed();
        return element.IsDisplayed();
    }
    
    doGetText(element)
    {
        element.waitForDisplayed();
        return element.getText();
    }

    doGetPageTitle()
    {
        return browser.getTitle();
    }
}
module.exports = new ElementUtil()