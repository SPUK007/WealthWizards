const { default: $ } = require('webdriverio/build/commands/browser/$');
const Page = require('../page');
const elementUtil = require('../util/elementUtil');
class leadQAPage  extends Page{
   
    getPageTitle()
    {
        return elementUtil.doGetPageTitle();        
    }
    
}
module.exports = new leadQAPage()