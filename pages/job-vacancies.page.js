const { default: $ } = require('webdriverio/build/commands/browser/$');
const Page = require('../page');
const elementUtil = require('../util/elementUtil');
class jobVacanicesPage  extends Page{

    get jobList() { return $("//div[@role='list']"); }
    get leadQA() { return $("//a[@href='/jobs/lead-quality-engineer']"); }

    getJobList()
    {
        return elementUtil.doGetText(this.jobList);
    }

    findOutMoreBtn()
    {
        elementUtil.doClick(leadQA);

    }
    
}
module.exports = new jobVacanicesPage()