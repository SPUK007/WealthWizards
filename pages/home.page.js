const { default: $ } = require('webdriverio/build/commands/browser/$');
const Page = require('../page');
const elementUtil = require('../util/elementUtil');

class HomePage  extends Page{

    get aboutUs() { return $("//div[@id='w-dropdown-toggle-1']//div[contains(text(),'About us')]"); }
    get whoWeAre() { return $("//a[normalize-space()='Who we are']"); }
    get careers() { return $("//nav[@id='w-dropdown-list-1']//a[@class='w-dropdown-link'][normalize-space()='Careers']"); }
    get contacts() {return $("//a[normalize-space()='Contact']"); }

    getPageTitle()
    {
        return elementUtil.doGetPageTitle();        
    }

    clickAboutUsBtn()
    {
        elementUtil.doClick(this.aboutUs);
    }

    clickCareersBtn()
    {
        elementUtil.doClick(this.careers);
    }


    isDoWhoWeAreDisplayed()
    {
        return elementUtil.doIsDisplayed(this.whoWeAre);
    }

    isDoCareersDisplayed()
    {
        return elementUtil.doIsDisplayed(this.careers);
    }

    isDoContactsDisplayed()
    {
        return elementUtil.doIsDisplayed(this.contacts);
    }

}
module.exports = new HomePage()