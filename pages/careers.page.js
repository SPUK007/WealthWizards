const { default: $ } = require('webdriverio/build/commands/browser/$');
const Page = require('../page');
class CareersPage  extends Page{

    get pioneering() { return $("//h4[normalize-space()='Pioneering']"); }
    get expert() { return $("//h4[normalize-space()='Expert']"); }
    get accessible() { return $("//h4[normalize-space()='Accessible']"); }
    get reliable() { return $("//h4[normalize-space()='Reliable']"); }
    get hungry() { return $("//h4[normalize-space()='Hungry']"); }
    get viewJobs() { return $("//div[contains(text(),'View jobs')]"); }

    isDopioneeringDisplayed()
    {
        return elementUtil.doIsDisplayed(this.pioneering);
    }

    isDoexpertDisplayed()
    {
        return elementUtil.doIsDisplayed(this.expert);
    }

    isDoaccessibleDisplayed()
    {
        return elementUtil.doIsDisplayed(this.accessible);
    }

    isDoreliableDisplayed()
    {
        return elementUtil.doIsDisplayed(this.reliable);
    }

    isDohungryDisplayed()
    {
        return elementUtil.doIsDisplayed(this.hungry);
    }

    isDoviewJobsDisplayed()
    {
        return elementUtil.doIsDisplayed(this.viewJobs);
    }

    clickviewJobsBtn()
    {
        elementUtil.doClick(this.viewJobs);
    }
    
}
module.exports = new CareersPage()