// const LoginPage = require('../../login.page');
// const SecurePage = require('../../secure.page');

const HomePage = require('../../pages/home.page');
const CareerPage = require('../../pages/careers.page');
const JVPage = require('../../pages/job-vacancies.page');
const leadQAPage = require('../../pages/lead-qualit-engineer.page');

const { assert } = require('console');

describe('WealthWizard - Lead QA Interview Automation Test', () => {
    
    it('WealthWizards home page: Page Title Verification', async () => {
        // browser.init();
        browser.url('/');
        browser.maximizeWindow();
        browser.pause(4000);
        
     // const title = HomePage.getPageTitle();
        const title = browser.getTitle;
        console.log("Home Page Title: "+title);
        // assert.equal('We are Wealth Wizards',title,'Title is not found!');
    });

    it ('To Check that About Us dropdown has 3 values: “Who we are”, “Careers” and “Contact”', async () =>{

        HomePage.clickAboutUsBtn;        
        return HomePage.isDoCareersDisplayed;
        return HomePage.isDoContactsDisplayed;
        return HomePage.isDoWhoWeAreDisplayed;
    });

    it ('Verify navigated to career page after Clicking on “Careers”', async () =>{
        HomePage.clickCareersBtn;
        // const title = HomePage.getPageTitle();
        const title = browser.getTitle();
        console.log("Career Page Title: "+title);
        // assert.equal('careers',title,'Title is not found!');
    });

    it ('Verify the 5  WW values', async () =>{
        CareerPage.isDopioneeringDisplayed;
        CareerPage.isDoreliableDisplayed;
        CareerPage.isDoexpertDisplayed;
        CareerPage.isDohungryDisplayed;
        CareerPage.isDoaccessibleDisplayed;        
    });

    it ('Verify the career page contains “View jobs” button', async () =>{
        CareerPage.isDoviewJobsDisplayed;
    });

    it ('Verify navigated to Job-Vacanies page after Clicking on “view-jobs”', async () =>{
        CareerPage.clickviewJobsBtn;
    });

    it ('Find “Lead Quality Engineer” role  and click “Find out more” button', async () =>{
        JVPage.getJobList;
        JVPage.findOutMoreBtn;
    });

    it ('Validate the Lead Quality Engineer job vacancy', async () =>{
        const title = leadQAPage.getPageTitle();
        console.log("Lead Quality Engineer job vacancy Page Title: "+title);
        // assert.equal('Jobs at Wealth Wizards | Lead Quality Engineer',title,'Title is not found!');
    });


});